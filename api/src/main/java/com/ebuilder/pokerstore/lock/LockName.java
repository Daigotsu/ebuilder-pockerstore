package com.ebuilder.pokerstore.lock;

/**
 * All possible locks name
 */
public enum LockName {
    CUSTOMER, PRODUCT
}
