package com.ebuilder.pokerstore;

import com.ebuilder.pokerstore.backoffice.BackOfficeService;
import com.ebuilder.pokerstore.backoffice.SupplierService;
import com.ebuilder.pokerstore.impl.service.BackOfficeServiceImpl;
import com.ebuilder.pokerstore.impl.service.StoreFrontServiceImpl;
import com.ebuilder.pokerstore.impl.service.SupplierServiceImpl;
import com.ebuilder.pokerstore.impl.repository.CustomerInMemoryRepository;
import com.ebuilder.pokerstore.impl.repository.StockInMemoryRepository;
import com.ebuilder.pokerstore.repository.CustomerRepository;
import com.ebuilder.pokerstore.repository.StockRepository;
import com.ebuilder.pokerstore.storefront.StoreFrontService;

/**
 * Construct beans for application w/o containers with DI.
 * Fully synchronized for correct infrastructure bean initialization
 */
public final class PokerstoreFactoryImpl implements PokerstoreFactory {

    private final CustomerRepository customerRepository = new CustomerInMemoryRepository();
    private final StockRepository stockRepository = new StockInMemoryRepository();

    private SupplierService supplierService;
    private BackOfficeService backOfficeService;
    private StoreFrontService storeFrontService;

    @Override
    public synchronized BackOfficeService createBackOfficeService() {
        if (backOfficeService == null) {
            backOfficeService = new BackOfficeServiceImpl(stockRepository, customerRepository);
        }
        return backOfficeService;
    }

    @Override
    public synchronized StoreFrontService createStoreFrontService() {
        if (storeFrontService == null) {
            storeFrontService =
                    new StoreFrontServiceImpl(stockRepository, customerRepository, getSupplierService(createBackOfficeService()));
        }
        return storeFrontService;
    }

    private SupplierService getSupplierService(final BackOfficeService backOfficeService) {
        if (supplierService == null) {
            supplierService = new SupplierServiceImpl(backOfficeService);
        }
        return supplierService;
    }
}
